# Naive Bayes classifier

This script enables to classify given dataset to one of three possible Iris plant classes based on 4 attributes.

**Possible classes:**
- Iris Setosa
- Iris Versicolour
- Iris Virginica

**Given attributes:**
- sepal length in cm
- sepal width in cm
- petal length in cm
- petal width in cm

More information about Naive Bayes classifier available 
[here](https://towardsdatascience.com/naive-bayes-classifier-81d512f50a7c).

## Requirements

    Python 3.8.8 (or higher)
    module numpy 1.19.5.
    module pandas 1.2.4.
    module matplotlib 3.3.4.

This script also requires Iris Dataset from UCI Machine Learning Repository:  
[Iris dataset](http://archive.ics.uci.edu/ml/datasets/Iris)


## How to run

    > python3 ./naive-bayes.py train_set

    Arguments:
        <float> train_set -> train set size


## Conclusions

Naive Bayes classifier is easy in implementation and fast in usage. It provides satisfying results. The best results could be reached in train:test split set on **80:20**.