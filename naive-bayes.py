"""
========================================================================================
    NAIVE BAYES CLASSIFIER

    Proper way to execute a script: python3 ./naive-bayes.py train_set
<float> train_set -> train set size

========================================================================================
"""


import sys
import math
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.options.mode.chained_assignment = None


"""
========================================================================================
CHECK_ARGS -> checking correctness of passed arguments
========================================================================================
"""


def check_args():
    if len(sys.argv) != 2:
        print('Error: Invalid number of arguments. Try: python3 ./naive-bayes.py train_set')
        print('Note that all passed arguments should be numbers.')
        sys.exit()

    fst_arg = float(sys.argv[1])
    if fst_arg < 0 or fst_arg > 1:
        print('Error: Invalid value of train_set argument.')
        sys.exit()


"""
========================================================================================
PREPARE_SETS -> preparing training and test sets
========================================================================================
"""


def prepare_sets(tr_size, dataset_):
    print("Training set will contain {:.2%} elements of whole dataset".format(tr_size))

    trainings = dataset_.sample(frac=training_size)
    dataset_ = dataset_.drop(trainings.index)
    tests = dataset_

    trainings.reset_index(drop=True, inplace=True)
    tests.reset_index(drop=True, inplace=True)
    tests.insert(len(columns), 'Predicted species', value=None)

    return [trainings, tests]


"""
========================================================================================
TRAIN -> calculate mean value and standard deviation 
========================================================================================
"""


def train(trainset, columns_):
    param_ = {}
    params = []
    cls_freq = []
    train_len = len(trainset.values)
    freq_ = trainset['Species'].value_counts()

    start_ = time.perf_counter_ns()

    for i in range(0, len(species)):
        cls_freq.append(freq_[i]/train_len)
        traindata = trainset[trainset["Species"] == species[i]]

        for j in range(0, len(columns_)-1):
            mean = traindata[columns_[j]].mean()
            stddev = traindata[columns_[j]].std()
            param_[columns_[j]] = [mean, stddev]

        params.append(param_.copy())

    stop_ = (time.perf_counter_ns() - start_) / (10 ** 9)
    print('Training time: {} s'.format(stop_))

    return [params, cls_freq]


"""
========================================================================================
GAUSS_DENSITY -> returns a probability density function value
========================================================================================
"""


def gauss_den(val_, params_):
    mean_ = params_[0]
    stddev_ = params_[1]

    gauss_fd = (1/(stddev_ * math.sqrt(2*math.pi))) * math.exp((-1*math.pow(val_ - mean_, 2))/(2*math.pow(stddev_, 2)))

    return gauss_fd


"""
========================================================================================
NAIVE_BAYES -> returns a probability of belonging to a class
========================================================================================
"""


def naive_bayes(element, params_, cls_freq):
    probs = np.empty([0, 0])

    for i in range(0, len(params_)):
        prob = 0
        for j in range(0, len(columns)-1):
            prob = prob + gauss_den(element[j], params_[i][columns[j]])

        prob = prob * cls_freq[i]
        probs = np.append(probs, prob)

    ind = np.where(probs == np.amax(probs))
    element['Predicted species'] = species[ind[0][0]]

    return element


"""
========================================================================================
GET_CONFUSION_MATRIX -> returns a confusion matrix
========================================================================================
"""


def get_confusion_matrix(tested_):
    classes_ = []
    matrix_ = np.empty([3, 3])

    for i in range(0, len(species)):
        classes_.append(tested_[tested_["Predicted species"] == species[i]])

    i = 0
    for cls in classes_:
        for j in range(0, len(species)):
            temp = cls[cls["Species"] == species[j]]
            matrix_[i][j] = len(temp.values)

        i = i + 1

    return matrix_


"""
========================================================================================
ACCURACY -> returns an accuracy ratio
========================================================================================
"""


def accuracy(tested):
    correct = 0

    for i in range(1, len(tested)):
        if tested['Species'].iloc[i] == tested['Predicted species'].iloc[i]:
            correct = correct + 1

    acc = correct / len(tested.values)
    print("Accuracy: {}/{} ({:.2%})".format(correct, len(tested.values), correct / len(tested.values)))
    return acc


"""
========================================================================================
RECALL -> returns an average recall ratio for classifier
========================================================================================
"""


def recall(matrix_):
    average_tpr = np.empty([0, 0])

    for i in range(0, len(species)):
        nom = matrix_[i][i]
        denom = 0

        for j in range(0, len(classes)):
            denom = denom + matrix_[j][i]

        tpr = nom / denom
        average_tpr = np.append(average_tpr, tpr)
        print("Recall for class {}: {}/{} ({:.2%})".format(species[i], int(nom), int(denom), tpr))

    average_tpr = average_tpr.mean()
    print("Average recall: {:.2%}\n".format(average_tpr))
    return average_tpr.mean


"""
========================================================================================
PRECISION -> returns an average precision ratio for classifier
========================================================================================
"""


def precision(matrix_):
    average_ppv = np.empty([0, 0])

    for i in range(0, len(species)):
        nom = matrix_[i][i]
        denom = 0

        for j in range(0, len(classes)):
            denom = denom + matrix_[i][j]

        ppv = nom / denom
        average_ppv = np.append(average_ppv, ppv)
        print("Precision for class {}: {}/{} ({:.2%})".format(species[i], int(nom), int(denom), ppv))

    average_ppv = average_ppv.mean()
    print("Average precision: {:.2%}\n".format(average_ppv))
    return average_ppv.mean


"""
========================================================================================
VISUALIZE -> scatter-plot for two choosen attributes

Attributes:
    0 - sepal length
    1 - sepal width
    2 - petal length
    3 - petal width
========================================================================================
"""


def visualize(classes_, species_, atr1, atr2):
    x_values = []
    y_values = []

    for cls in classes_:
        x_values.append(cls[columns[atr1]].values)
        y_values.append(cls[columns[atr2]].values)

    plt.scatter(x_values[0], y_values[0], c='#ff0000')
    plt.scatter(x_values[1], y_values[1], c='#ffa500')
    plt.scatter(x_values[2], y_values[2], c='#0000ff')
    plt.xlabel(columns[atr1])
    plt.ylabel(columns[atr2])
    plt.legend(species_)
    plt.grid()
    plt.show()

    return 0


"""
========================================================================================

========================================================================================
"""

check_args()
training_size = float(sys.argv[1])

columns = ['Sepal length', 'Sepal width', 'Petal length', 'Petal width', 'Species']
dataset = pd.read_csv('iris.csv', names=columns)

species = dataset['Species'].unique()
print('\n*** Possible species in dataset: {} ***'.format(species))

classes = []
for spec in species:
    classes.append(dataset[dataset['Species'] == spec])

[train_set, test_set] = prepare_sets(training_size, dataset)
[parameters, class_prob] = train(train_set, columns)

print('\n*** Classifying all elements in test set ***')
start = time.perf_counter_ns()

for it in range(0, len(test_set)):
    test_set.iloc[it] = naive_bayes(test_set.iloc[it], parameters, class_prob)

stop = (time.perf_counter_ns() - start) / (10 ** 9)
print('Classifying time: {} s\n'.format(stop))

print('*** Performance ***\n')
confusion_matrix = get_confusion_matrix(test_set)
print('Confusion matrix:\n {}\n'.format(confusion_matrix))

accuracy(test_set)
recall(confusion_matrix)
precision(confusion_matrix)

"""
========================================================================================
visualize(classes, species, 0, 1)
visualize(classes, species, 0, 2)
visualize(classes, species, 0, 3)
visualize(classes, species, 1, 2)
visualize(classes, species, 1, 3)
visualize(classes, species, 2, 3)
========================================================================================
"""
